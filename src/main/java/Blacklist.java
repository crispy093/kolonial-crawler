import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class Blacklist {

  List<String> blacklistedStrings;

  public Blacklist() {
    try {
      InputStream input = getClass().getResourceAsStream("BlacklistedStrings.txt");

      String blacklistedStringsRaw = IOUtils.toString(input, StandardCharsets.UTF_8.name());

      blacklistedStrings = Arrays.asList(blacklistedStringsRaw.split(",", -1));
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  public boolean isInBlacklist(String stringToCheck) {
    for (String string : blacklistedStrings) {
      if (stringToCheck.contains(string)) {
        return true;
      }
    }

    return false;
  }

  public List<String> getBlacklistedStrings() {
    return blacklistedStrings;
  }
}
