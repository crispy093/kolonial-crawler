import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import com.google.gson.Gson;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebCrawler {

  private Blacklist blacklist;
  private int maxDepth;
  private HashSet<String> links;
  private HashSet<Product> products;
  private Properties prop;

  public WebCrawler(int maxDepth, Properties prop) {
    blacklist = new Blacklist();
    links = new HashSet<String>();
    products = new HashSet<Product>();
    this.maxDepth = maxDepth;
    this.prop = prop;
  }

  public HashSet<Product> getProducts() {
    return products;
  }

  public void getPageLinks(String URL, int depth) {
    if (!links.contains(URL) && depth < maxDepth) {
      try {
        links.add(URL);
        System.out.println(URL);

        // Get the page
        Document document = Jsoup.connect(URL).get();

        // If current doc is in the "products" subdomain, try to transform it into object
        if (URL.contains(prop.getProperty("ProductsSubUrl"))
            && !URL.endsWith(prop.getProperty("ProductsSubUrl"))) {
          addNewProduct(URL, document);
        }

        // Get all links on page
        Elements linksOnPage = document.select("a[href]");

        // Increase current depth
        depth++;

        // Follow all page links
        for (Element page : linksOnPage) {
          String href = page.attr("abs:href");
          if (href.contains(prop.getProperty("Domain")) && !blacklist.isInBlacklist(href)) {
            getPageLinks(page.attr("abs:href"), depth);
          }
        }
      } catch (IOException e) {
        System.out.println(URL + " produced error: " + e.getMessage());
      }
    }
  }

  private void addNewProduct(String URL, Document document) {

    // Get rid of the brand pages
    if (!document.html().contains("Kjøp varer fra")) {
      try {
        Product newProduct = new Product(URL, document.html());
        String ignore = "Kjøp varer fra";
        products.add(newProduct);
        System.out.println("Found " + products.size() + " products so far.");
      } catch (Exception e) {
        System.out.println(
            "Error occurred while trying to transform "
                + URL
                + " to product object - "
                + e.getMessage());
      }
    }
  }

  private void exportToJson() {

    System.out.println("Exporting to json");
    String filePath = prop.getProperty("OutputFile");
    Gson gson = new Gson();
    try {
      gson.toJson(products, new FileWriter(filePath));
      System.out.println("Done - " + filePath);
    } catch (Exception e) {
      System.out.println("Exporting failed.");
      System.out.println(e.getMessage());
    }
  }

  public static void main(String[] args) {

    try (InputStream propFile =
        WebCrawler.class.getClassLoader().getResourceAsStream("config.properties")) {
      Properties prop = new Properties();
      prop.load(propFile);

      double startTime = System.currentTimeMillis();

      WebCrawler crawler = new WebCrawler(Integer.parseInt(prop.getProperty("MaxDepth")), prop);

      crawler.getPageLinks(prop.getProperty("URL"), 0);

      double endTime = System.currentTimeMillis();

      double secondsTaken = (endTime - startTime) / 1000;

      System.out.println(
          "Found " + crawler.getProducts().size() + " products in " + secondsTaken + "seconds.");

      crawler.exportToJson();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
