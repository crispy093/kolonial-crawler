import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Product {
  String name;
  String description;
  String price;
  String currency;
  String url;

  public Product(String url, String html) {
    this.url = url;

    extractMetadata(html);

    System.out.println("New Product: " + name);
  }

  private void extractMetadata(String html) {
    Document doc = Jsoup.parse(html);
    Elements metaTags = doc.getElementsByTag("meta");
    for (Element metaTag : metaTags) {
      if (metaTag.attr("property").equals("og:title")) {
        name = metaTag.attr("content");
      } else if (metaTag.attr("property").equals("og:description")) {
        description = metaTag.attr("content");
      } else if (metaTag.attr("property").equals("product:price:amount")) {
        price = metaTag.attr("content");
      } else if (metaTag.attr("property").equals("product:price:currency")) {
        currency = metaTag.attr("content");
      }
    }
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public String getPrice() {
    return price;
  }

  public String getCurrency() {
    return currency;
  }

  public String getUrl() {
    return url;
  }
}
