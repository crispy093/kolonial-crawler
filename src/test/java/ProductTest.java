import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

  @Test
  void testCousCous() {
    try {
      File input = new File("src/test/resources/couscoustest.html");
      String testHtml = Files.readString(Path.of("src/test/resources/couscoustest.html"));

      Product testCousCous = new Product("www.test.com", testHtml);

      assertTrue(testCousCous.getName().equals("Go Green Fullkornscouscous 400 g"));
      assertTrue(testCousCous.getDescription().equals("CousCousTest"));
      assertTrue(testCousCous.getPrice().equals("100.0"));
      assertTrue(testCousCous.getCurrency().equals("NOK"));

    } catch (Exception e) {
      System.out.println(e.getMessage());
      assertTrue(false);
    }
  }
}
